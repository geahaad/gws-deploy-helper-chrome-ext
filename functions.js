/********************************************************
	Google Chrome Extension for additional GWS Deploy Tool Features
	-------------------------------------------------
	(c) Blue IT-Services Ges.m.b.H.
	-------------------------------------------------
	2013 Gerhard Dinhof
********************************************************/

var GWS = GWS || {};

GWS.deploy = {
	clickHandler: function(app, env, version, delay) {
		delay = typeof delay !== 'undefined' ? delay : 0;
		var answer = confirm("Deploy Version " + version + " to " + env + "\n\nAre you sure?");
		if (answer) {
			// do deployment
			$.post("deployAction.do?app=" + app + "&env=" + env + "&opt=deploy", { 'version': version, 'delay': delay });
			console.log("deploy started: " + app + " / " + env + " / " + version);
			window.location = "status.do";
		}
	},

	createButton: function(app, env, version, qsEnv, delay) {
		delay = typeof delay !== 'undefined' ? delay : 0;
		return $('<button/>', {
			text: GWS.deploy.buttonString(version, delay),
			disabled: GWS.deploy.isDeployRunning(qsEnv),
			click: function() {
					GWS.deploy.clickHandler(app, env, version, delay)
				}
			}
		);
	},

	buttonString: function(version, delay) {
		delay = typeof delay !== 'undefined' ? delay : 0;
		if (delay == 0)
			return 'Deploy ' + version + ' from HF';
		else
			return 'Deploy ' + version + ' from HF in ' + delay + ' minutes';
	},

	isDeployRunning: function(selector) {
		  return $($('a[title*="' + selector + '"]').parent().siblings('td')[2]).find('img').length > 0;
	}
}