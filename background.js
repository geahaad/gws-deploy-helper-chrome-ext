/********************************************************
	Google Chrome Extension for additional GWS Deploy Tool Features
	-------------------------------------------------
	(c) Blue IT-Services Ges.m.b.H.
	-------------------------------------------------
	2013 Gerhard Dinhof
********************************************************/

/**** General settings ****/
var ccpQsEnv = "[GWS Customer Care Portal] - QS Umgebung";
var ccpQsPilotEnv = "[GWS Customer Care Portal] - QS-Pilotumgebung";
var ccpQsPilot2Env = "[GWS Customer Care Portal] - QS-PILOT2";
var gwsQsEnv = "[Group Web Solutions] - QS Umgebung";
var gwsQsPilotEnv = "[Group Web Solutions] - QS-Pilotumgebung";
var gwsQsPilot2Env = "[Group Web Solutions] - QS-PILOT2";
var dcsQsEnv = "[GWS Webservice - Java Web Banking Service] - QS Umgebung";
var dcsQsPilotEnv = "[GWS Webservice - Java Web Banking Service] - QS-Pilotumgebung";
var dcsQsPilot2Env = "[GWS Webservice - Java Web Banking Service] - QS-PILOT2";

/**** CCP *****/
var ccpHfVersionString = $('a[title*="[GWS Customer Care Portal] - HF Umgebung"]').text();
var ccpHfVersionNumber = ccpHfVersionString.slice(0, ccpHfVersionString.indexOf('.'))
var ccpProdBtn = GWS.deploy.createButton('ccp', 'qs', ccpHfVersionNumber, ccpQsEnv);
var ccpProdBtnDelay = GWS.deploy.createButton('ccp', 'qs', ccpHfVersionNumber, ccpQsEnv, 10);
$('a[title*="' + ccpQsEnv + '"]').after(ccpProdBtnDelay).after("<br/>");
$('a[title*="' + ccpQsEnv + '"]').after(ccpProdBtn).after("<br/>");

var ccpPilotBtn = GWS.deploy.createButton('ccp', 'qs-pilot', ccpHfVersionNumber, ccpQsEnv);
$('a[title*="' + ccpQsPilotEnv + '"]').after(ccpPilotBtn).after("<br/>");

var ccpPilot2Btn = GWS.deploy.createButton('ccp', 'qs-pilot2', ccpHfVersionNumber, ccpQsEnv);
$('a[title*="' + ccpQsPilot2Env + '"]').after(ccpPilot2Btn).after("<br/>");

/**** GWS *****/
var gwsHfVersionString = $('a[title*="[Group Web Solutions] - Hotfix"]').text();
var gwsHfVersionNumber = gwsHfVersionString.slice(0, gwsHfVersionString.indexOf('.'))
var gwsProdBtn = GWS.deploy.createButton('gws', 'qs', gwsHfVersionNumber, gwsQsEnv);
var gwsProdBtnDelay = GWS.deploy.createButton('gws', 'qs', gwsHfVersionNumber, gwsQsEnv, 5);
$('a[title*="' + gwsQsEnv + '"]').after(gwsProdBtnDelay).after("<br/>");
$('a[title*="' + gwsQsEnv + '"]').after(gwsProdBtn).after("<br/>");

var gwsPilotBtn = GWS.deploy.createButton('gws', 'qs-pilot', gwsHfVersionNumber, gwsQsEnv);
$('a[title*="' + gwsQsPilotEnv + '"]').after(gwsPilotBtn).after("<br/>");

var gwsPilot2Btn = GWS.deploy.createButton('gws', 'qs-pilot2', gwsHfVersionNumber, gwsQsEnv);
$('a[title*="' + gwsQsPilot2Env + '"]').after(gwsPilot2Btn).after("<br/>");

/**** DCS *****/
var dcsHfVersionString = $('a[title*="[GWS Webservice - Java Web Banking Service] - HF-Umgebung"]').text();
var dcsHfVersionNumber = dcsHfVersionString.slice(0, dcsHfVersionString.indexOf('.'))
var dcsProdBtn = GWS.deploy.createButton('jwbs', 'qs', dcsHfVersionNumber, dcsQsEnv);
$('a[title*="' + dcsQsEnv + '"]').after(dcsProdBtn).after("<br/>");

var dcsPilotBtn = GWS.deploy.createButton('jwbs', 'qs-pilot', dcsHfVersionNumber, dcsQsEnv);
$('a[title*="' + dcsQsPilotEnv + '"]').after(dcsPilotBtn).after("<br/>");

var dcsPilot2Btn = GWS.deploy.createButton('jwbs', 'qs-pilot2', dcsHfVersionNumber, dcsQsEnv);
$('a[title*="' + dcsQsPilot2Env + '"]').after(dcsPilot2Btn).after("<br/>");